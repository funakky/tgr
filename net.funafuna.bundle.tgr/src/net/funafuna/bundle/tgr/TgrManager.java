package net.funafuna.bundle.tgr;

import javax.servlet.ServletException;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.http.HttpService;
import org.osgi.service.http.NamespaceException;
import org.osgi.util.tracker.ServiceTracker;
import org.osgi.util.tracker.ServiceTrackerCustomizer;

public class TgrManager implements BundleActivator, ServiceTrackerCustomizer<HttpService, HttpService> {

	private BundleContext context;
	
	private ServiceTracker<HttpService, HttpService> tracker;

	public void start(BundleContext context) throws Exception {
		this.context = context;
		this.tracker = new ServiceTracker<>(context, HttpService.class, this);
		this.tracker.open();
	}

	public void stop(BundleContext context) throws Exception {
		this.tracker.close();
		this.tracker = null;
		this.context = null;
	}

	@Override
	public HttpService addingService(ServiceReference<HttpService> reference) {
		HttpService service = this.context.getService(reference);
		try {
			service.registerServlet("/tgr", new TgrServlet(), null, null);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (NamespaceException e) {
			e.printStackTrace();
		}
		return service;
	}

	@Override
	public void modifiedService(ServiceReference<HttpService> reference,
			HttpService service) {
	}

	@Override
	public void removedService(ServiceReference<HttpService> reference,
			HttpService service) {
		service.unregister("/tgr");
	}

}
